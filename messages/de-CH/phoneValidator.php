<?php return [
	'Unexpected Phone Number Format or Country Code' => 'Unerwartetes Nummernformat oder Landesvorwahl.',
	'Unexpected Phone Number Format' => 'Unerwartetes Nummernformat.',
	'Phone number does not seem to be a valid phone number' => 'Scheint keine Telefonnummer zu sein.',
	'For phone validation country required' => 'Landesvorwahl benötigt.'
];
