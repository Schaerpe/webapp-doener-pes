<?php

use yii\db\Migration;

/**
 * Class m200106_133630_create_table_collectiveOrder
 */
class m200106_133630_create_table_collectiveOrder extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%collective-order}}', [
			'id'=>$this->primaryKey(),
			'supplier_id'=>$this->integer()->notNull(),
			'responsible_user_id'=>$this->integer()->notNull(),
			'created_at'=>$this->integer()->notNull(),
			'deadline'=>$this->integer()->notNull(),
			'paid'=>$this->boolean()->defaultValue(false),
		]);

		$this->addForeignKey(
			'fk_collectiveOrder_supplier_supplier_id',
			'{{%collective-order}}',
			'supplier_id',
			'{{%supplier}}',
			'id',
			'CASCADE',
			'CASCADE');

		$this->addForeignKey(
			'fk_collectiveOrder_user_responsible_user_id',
			'{{%collective-order}}',
			'responsible_user_id',
			'{{%user}}',
			'id',
			'CASCADE',
			'CASCADE'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropTable('{{%collective-order}}');
    }

}
