<?php

use yii\db\Migration;

/**
 * Class m200106_135313_create_table_order
 */
class m200106_135313_create_table_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%order}}', [
			'id'=>$this->primaryKey(),
			'user_id'=>$this->integer()->notNull(),
			'product_id'=>$this->integer()->notNull(),
			'collectiveorder_id'=>$this->integer()->notNull(),
			'total_price'=>$this->float(2),
			'notes'=>$this->string(255),
		]);

		$this->addForeignKey(
			'fk_order_user_user_id',
			'{{%order}}',
			'user_id',
			'{{%user}}',
			'id',
			'CASCADE',
			'CASCADE'
		);

		$this->addForeignKey(
			'fk_order_product_product_id',
			'{{%order}}',
			'product_id',
			'{{%product}}',
			'id',
			'CASCADE',
			'CASCADE'
		);

		$this->addForeignKey(
			'fk_order_collectiveOrder_collectiveorder_id',
			'{{%order}}',
			'collectiveorder_id',
			'{{%collective-order}}',
			'id',
			'CASCADE',
			'CASCADE'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropTable('{{%order}}');
    }

}
