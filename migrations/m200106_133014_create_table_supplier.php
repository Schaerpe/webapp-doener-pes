<?php

use yii\db\Migration;

/**
 * Class m200106_133014_create_table_supplier
 */
class m200106_133014_create_table_supplier extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('supplier', [
			'id'=>$this->primaryKey(),
			'name'=>$this->string(128)->notNull(),
			'email'=>$this->string(128),
			'address'=>$this->string(255)->notNull(),
			'phone'=>$this->string(128),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%supplier}}');
    }
}
