<?php

use yii\db\Migration;

/**
 * Class m200106_134713_create_table_product
 */
class m200106_134713_create_table_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%product}}', [
			'id'=>$this->primaryKey(),
			'supplier_id'=>$this->integer()->notNull(),
			'name'=>$this->string(128)->notNull(),
			'base_price'=>$this->float(2)->notNull(),
		]);

		$this->addForeignKey(
			'fk_product_supplier_supplier_id',
			'{{%product}}',
			'supplier_id',
			'{{%supplier}}',
			'id',
			'CASCADE',
			'CASCADE'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    	$this->dropTable('{{%product}}');
    }

}
