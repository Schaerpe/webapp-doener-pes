<?php

use yii\db\Migration;

/**
 * Class m200106_140115_create_table_productIngredient
 */
class m200106_140115_create_table_productIngredient extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%productIngredient}}', [
			'id'=>$this->primaryKey(),
			'product_id'=>$this->integer()->notNull(),
			'ingredient_id'=>$this->integer()->notNull(),
			'additional_price'=>$this->float(2),
		]);

	    $this->addForeignKey(
		    'fk_productIngredient_product_product_id',
		    '{{%productIngredient}}',
		    'product_id',
		    '{{%product}}',
		    'id',
		    'CASCADE',
		    'CASCADE'
	    );

	    $this->addForeignKey(
	    	'fk_productIngredient_ingredient_ingredient_id',
		    '{{%productIngredient}}',
		    'ingredient_id',
		    '{{%ingredient}}',
		    'id',
		    'CASCADE',
		    'CASCADE'
	    );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropTable('{{%productIngredient}}');
    }

}
