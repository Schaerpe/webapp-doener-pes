<?php

use yii\db\Migration;

/**
 * Class m200106_133342_create_table_ingredient
 */
class m200106_133342_create_table_ingredient extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%ingredient}}', [
			'id'=>$this->primaryKey(),
			'name'=>$this->string(128)->notNull(),
			'description'=>$this->string(255),
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ingredient}}');
    }

}
