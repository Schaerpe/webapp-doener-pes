<?php

use yii\db\Migration;

/**
 * Class m200106_132408_create_table_user
 */
class m200106_132408_create_table_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->createTable('{{%user}}', [
		    'id'=>$this->primaryKey(),
		    'short'=>$this->string(3)->notNull(),
		    'email'=>$this->string(128)->notNull(),
		    'auth_key'=>$this->string(32),
		    'password_hash'=>$this->string()->notNull(),
		    'password_reset_token'=>$this->string(255),
		    'is_locked'=>$this->boolean()->notNull()->defaultValue(0),
		    'is_archived'=>$this->boolean()->notNull()->defaultValue(0),
	    ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropTable('{{%user}}');
    }

}
