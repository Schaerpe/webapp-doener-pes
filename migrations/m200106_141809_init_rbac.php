<?php

use yii\db\Migration;

/**
 * Class m200106_141809_init_rbac
 */
class m200106_141809_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$auth = Yii::$app->authManager;

		$regular = $auth->createRole('regular');
		$auth->add($regular);

	    $admin = $auth->createRole('admin');
	    $auth->add($admin);
	    $auth->addChild($admin, $regular);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$auth = Yii::$app->authManager;
		$auth->removeAll();
    }

}
