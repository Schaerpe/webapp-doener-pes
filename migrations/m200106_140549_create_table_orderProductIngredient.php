<?php

use yii\db\Migration;

/**
 * Class m200106_140549_create_table_orderProductIngredient
 */
class m200106_140549_create_table_orderProductIngredient extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%orderProductIngredient}}', [
			'id'=>$this->primaryKey(),
			'order_id'=>$this->integer()->notNull(),
			'productingredient_id'=>$this->integer()->notNull(),
		]);

		$this->addForeignKey(
			'fk_orderProductIngredient_order_order_id',
			'{{%orderProductIngredient}}',
			'order_id',
			'{{%order}}',
			'id',
			'CASCADE',
			'CASCADE'
		);

		$this->addForeignKey(
			'fk_orderProductIngredient_productIngredient_productingredient_id',
			'{{%orderProductIngredient}}',
			'productingredient_id',
			'{{%productIngredient}}',
			'id',
			'CASCADE',
			'CASCADE'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		$this->dropTable('{{%orderProductIngredient}}');
    }

}
