<?php

namespace app\components;

class ActiveQuery extends \yii\db\ActiveQuery
{

	/**
	 * Process a filter param and return an array containing only field values.
	 *
	 * @param \yii\base\Model|\yii\base\Model[]|mixed|mixed[] $value either a single value/model or an array
	 * @param string $field the field name to extract the value from in case it is a model
	 *
	 * @return array list of primitive values
	 */
	protected static function processFilter($value, $field)
	{
		if (!is_array($value)) {
			$value = [$value];
		}

		return array_map(function ($v) use ($field) {
			return ($v instanceof \yii\base\Model) ? $v->$field : $v;
		}, $value);
	}

}
