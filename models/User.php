<?php

namespace app\models;

use app\models\query\UserQuery;
use Yii;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $short
 * @property string $email
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property int $is_locked
 * @property int $is_archived
 *
 * @property Collectiveorder[] $collectiveorders
 * @property Order[] $orders
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['short', 'email'], 'required'],
            [['is_locked', 'is_archived'], 'integer'],
            [['short'], 'string', 'max' => 3],
            [['email'], 'string', 'max' => 128],
            [['auth_key'], 'string', 'max' => 32],
            [['password_hash', 'password_reset_token'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short' => 'Kürzel',
            'email' => 'E-Mail',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'is_locked' => 'Is Locked',
            'is_archived' => 'Is Archived',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectiveorders()
    {
        return $this->hasMany(Collectiveorder::className(), ['responsible_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['user_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

	/**
	 * Validates password
	 *
	 * @param $password string
	 * @return bool whether or not password is correct for this user
	 */
    public function validatePassword($password) {
    	return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

	/**
	 * @inheritDoc
	 */
	public static function findIdentity($id)
	{
		return User::find()->id($id)->one();
	}

	/**
	 * @inheritDoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return User::find()->accessToken($token)->one();
	}

	/**
	 * @inheritDoc
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @inheritDoc
	 */
	public function getAuthKey()
	{
		return $this->auth_key;
	}

	/**
	 * @inheritDoc
	 */
	public function validateAuthKey($authKey)
	{
		return $authKey === $this->auth_key;
	}

}
