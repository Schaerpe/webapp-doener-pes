<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productingredient".
 *
 * @property int $id
 * @property int $product_id
 * @property int $ingredient_id
 * @property float|null $additional_price
 *
 * @property Orderproductingredient[] $orderproductingredients
 * @property Ingredient $ingredient
 * @property Product $product
 */
class ProductIngredient extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productingredient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'ingredient_id'], 'required'],
            [['product_id', 'ingredient_id'], 'integer'],
            [['additional_price'], 'number'],
            [['ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['ingredient_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'ingredient_id' => 'Ingredient ID',
            'additional_price' => 'Additional Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderproductingredients()
    {
        return $this->hasMany(Orderproductingredient::className(), ['productingredient_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(Ingredient::className(), ['id' => 'ingredient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * {@inheritdoc}
     * @return ProductIngredientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductIngredientQuery(get_called_class());
    }
}
