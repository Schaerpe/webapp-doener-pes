<?php

namespace app\models\query;

use app\models\CollectiveOrder;
use DateTime;

/**
 * This is the ActiveQuery class for [[CollectiveOrder]].
 *
 * @see CollectiveOrder
 */
class CollectiveOrderQuery extends \yii\db\ActiveQuery
{

    /**
     * {@inheritdoc}
     * @return CollectiveOrder[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CollectiveOrder|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	/**
	 * Named scope to find collective orders where the deadlines are either reached already or not reached yet.
	 *
	 * @param bool $reached DateTime|int whether or not to find finalized orders
	 * @return \app\models\query\CollectiveOrderQuery
	 */
    public function deadlineReached($reached=true)
    {
    	$timestampNow = time();
    	if ($reached) {
		    return $this->andWhere(['>=', 'deadline', $timestampNow]);
	    } else {
		    return $this->andWhere(['<', 'deadline', $timestampNow]);
	    }
    }

}
