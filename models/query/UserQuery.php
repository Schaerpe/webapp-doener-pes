<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @see User
 */
class UserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return User[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return User|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

	/**
	 * Named scope to find user by short (ex. PES)
	 *
	 * @param string $short
	 * @return \app\models\query\UserQuery
	 */
    public function short($short)
    {
    	return $this->andWhere(['short'=>strtoupper($short)]);
    }

	/**
	 * Named scope to find user by id
	 *
	 * @param $id
	 * @return \app\models\query\UserQuery
	 */
    public function id($id)
    {
	    return $this->andWhere(['id'=>intval($id)]);
    }

    public function accessToken($token)
    {
    	return $this->andWhere(['auth_key'=>$token]);
    }

}
