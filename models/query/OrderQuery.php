<?php

namespace app\models\query;

use app\components\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Order]].
 *
 * @see Order
 */
class OrderQuery extends ActiveQuery
{

	public function collectiveOrder($collectiveOrder)
	{
		$collectiveOrderIds = static::processFilter($collectiveOrder, 'id');
		return $this->andWhere(['collectiveorder_id'=>$collectiveOrderIds]);
	}

}
