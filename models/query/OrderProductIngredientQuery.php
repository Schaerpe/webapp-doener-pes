<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[OrderProductIngredient]].
 *
 * @see OrderProductIngredient
 */
class OrderProductIngredientQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return OrderProductIngredient[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return OrderProductIngredient|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
