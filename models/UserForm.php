<?php

namespace app\models;

class UserForm extends User
{

	public $password = '';

	public function rules()
	{
		return array_merge(parent::rules(), [
			[['password'], 'trim'],
			[['password'], 'string', 'min' => 6],
		]);
	}

	public function attributeLabels()
	{
		return array_merge(parent::attributeLabels(), [
			'password' => 'Passwort',
		]);
	}


	public function afterValidate()
	{
		$this->short = strtoupper($this->short);

		try {
			$this->password_hash = \Yii::$app->security->generatePasswordHash($this->password);
		} catch (\Exception $e) {
			$this->addError('password', 'Es gab einen unerwarteten Fehler beim Erstellen des Passwort-Hashs.');
		}
	}

}
