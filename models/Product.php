<?php

namespace app\models;

use app\models\query\ProductQuery;
use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $supplier_id
 * @property string $name
 * @property float $base_price
 *
 * @property Order[] $orders
 * @property Supplier $supplier
 * @property Productingredient[] $productingredients
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplier_id', 'name', 'base_price'], 'required'],
            [['supplier_id'], 'integer'],
            [['base_price'], 'number'],
            [['name'], 'string', 'max' => 128],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplier_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supplier_id' => 'Supplier ID',
            'name' => 'Name',
            'base_price' => 'Base Price',
        ];
    }

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductingredients()
    {
        return $this->hasMany(Productingredient::className(), ['product_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }
}
