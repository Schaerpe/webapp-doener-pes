<?php

namespace app\models;

use app\models\query\CollectiveOrderQuery;
use DateTime;
use Yii;

/**
 * This is the model class for table "collectiveorder".
 *
 * @property int $id
 * @property int $supplier_id
 * @property int $responsible_user_id
 * @property int $created_at
 * @property int $deadline
 * @property int|null $paid
 *
 * @property Supplier $supplier
 * @property User $responsibleUser
 * @property Order[] $orders
 */
class CollectiveOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'collectiveorder';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplier_id', 'deadline'], 'required'],

            [['supplier_id', 'responsible_user_id', 'created_at', 'paid'], 'integer'],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplier_id' => 'id']],

	        [['responsible_user_id'], 'default', 'value'=>Yii::$app->user->identity->id],
	        [['created_at'], 'default', 'value'=>time()],

	        [['deadline'], function () {
        	    $time = DateTime::createFromFormat('G.i', $this->deadline);

        	    if ($time === false) {
        	    	$this->addError('deadline', 'Zeitformat ungültig. Bsp. 11.30');
        	    	return;
	            }

        	    if ($time->getTimestamp() < time()) {
        	    	$this->addError('deadline', 'Zeit darf nicht in der Vergangenheit liegen');
	            }

        	    $this->deadline = $time->getTimestamp();
	        }]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'supplier_id' => 'Supplier ID',
            'responsible_user_id' => 'Responsible User ID',
            'created_at' => 'Created At',
            'deadline' => 'Deadline',
            'paid' => 'Paid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Supplier::className(), ['id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsibleUser()
    {
        return $this->hasOne(User::className(), ['id' => 'responsible_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['collectiveorder_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return CollectiveOrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CollectiveOrderQuery(get_called_class());
    }
}
