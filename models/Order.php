<?php
namespace app\models;

use app\models\query\OrderQuery;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $user_id
 * @property int $product_id
 * @property int $collectiveorder_id
 * @property float|null $total_price
 * @property string|null $notes
 *
 * @property Collectiveorder $collectiveorder
 * @property Product $product
 * @property User $user
 * @property Orderproductingredient[] $orderproductingredients
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'product_id', 'collectiveorder_id'], 'required'],
            [['user_id', 'product_id', 'collectiveorder_id'], 'integer'],
            [['total_price'], 'number'],
            [['notes'], 'string', 'max' => 255],
            [['collectiveorder_id'], 'exist', 'skipOnError' => true, 'targetClass' => Collectiveorder::className(), 'targetAttribute' => ['collectiveorder_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'product_id' => 'Product ID',
            'collectiveorder_id' => 'Collectiveorder ID',
            'total_price' => 'Total Price',
            'notes' => 'Notes',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectiveorder()
    {
        return $this->hasOne(Collectiveorder::className(), ['id' => 'collectiveorder_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderproductingredients()
    {
        return $this->hasMany(Orderproductingredient::className(), ['order_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return OrderQu the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }




}
