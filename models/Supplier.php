<?php

namespace app\models;

use app\models\query\SupplierQuery;
use Yii;

/**
 * This is the model class for table "supplier".
 *
 * @property int $id
 * @property string $name
 * @property string|null $email
 * @property string $address
 * @property string|null $phone
 *
 * @property Collectiveorder[] $collectiveorders
 * @property Product[] $products
 */
class Supplier extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'supplier';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
        	[['name', 'address', 'email', 'phone'], 'trim'],

            [['name', 'address'], 'required'],
            [['name', 'phone'], 'string', 'max' => 128],
            [['address'], 'string', 'max' => 255],
	        [['email'], 'email'],
	        [['phone'], 'udokmeci\yii2PhoneValidator\PhoneValidator', 'country'=>'CH']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'E-Mail',
            'address' => 'Adresse',
            'phone' => 'Telefon Nr.',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectiveorders()
    {
        return $this->hasMany(Collectiveorder::className(), ['supplier_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['supplier_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return SupplierQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SupplierQuery(get_called_class());
    }

}
