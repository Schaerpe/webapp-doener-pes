<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orderproductingredient".
 *
 * @property int $id
 * @property int $order_id
 * @property int $productingredient_id
 *
 * @property Order $order
 * @property Productingredient $productingredient
 */
class OrderProductIngredient extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orderproductingredient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'productingredient_id'], 'required'],
            [['order_id', 'productingredient_id'], 'integer'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['productingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Productingredient::className(), 'targetAttribute' => ['productingredient_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'productingredient_id' => 'Productingredient ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductingredient()
    {
        return $this->hasOne(Productingredient::className(), ['id' => 'productingredient_id']);
    }

    /**
     * {@inheritdoc}
     * @return OrderProductIngredientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderProductIngredientQuery(get_called_class());
    }
}
