<?php

namespace app\controllers;

use app\models\Product;
use Yii;
use app\models\Supplier;
use app\models\search\SupplierSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SupplierController implements the CRUD actions for Supplier model.
 */
class SupplierController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Supplier models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SupplierSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Supplier model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Supplier model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
	    $supplierModel = new Supplier();

	    if ($supplierModel->load(Yii::$app->request->post()) && $supplierModel->save()) {
		    //product models
		    $numberProducts = count(Yii::$app->request->post('Product', []));

		    for ($i = 0; $i < $numberProducts; $i++) {
		    	$newProduct = new Product();
		    	$newProduct->supplier_id = $supplierModel->id;
			    $products[] = $newProduct;
		    }

		    if (Product::loadMultiple($products, Yii::$app->request->post())
			    && Product::validateMultiple($products)) {
			    /** @var Product $product */
			    foreach ($products as $product) {
				    if (!$product->save(false)) {
					    $this->goBack();
					    Yii::$app->session->setFlash('error', 'Es ist ein Problem beim erstellen der Produkte aufgetaucht.');
				    }
			    }
			    return $this->redirect(['view', 'id' => $supplierModel->id]);
		    }
	    }

		$products = [new Product()];

        return $this->render('create', [
            'supplierModel' => $supplierModel,
	        'productModels' => $products,
        ]);
    }

    /**
     * Updates an existing Supplier model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
	    $supplierModel = $this->findModel($id);
	    $query = Product::find()->andWhere(['supplier_id' => $id]);
	    $productDataProvider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
			    'pageSize' => 10,
		    ],
	    ]);

	    if ($supplierModel->load(Yii::$app->request->post()) && $supplierModel->save()) {
		    //product models
		    $numberProducts = count(Yii::$app->request->post('Product', []));

		    for ($i = 0; $i < $numberProducts; $i++) {
			    $newProduct = new Product();
			    $newProduct->supplier_id = $supplierModel->id;
			    $products[] = $newProduct;
		    }

		    if (Product::loadMultiple($products, Yii::$app->request->post())
			    && Product::validateMultiple($products)) {
			    /** @var Product $product */
			    foreach ($products as $product) {
				    if (!$product->save(false)) {
					    $this->goBack();
					    Yii::$app->session->setFlash('error', 'Es ist ein Problem beim erstellen der Produkte aufgetaucht.');
				    }
			    }
		    }
		    return $this->goBack();
	    }

	    $products = [new Product()];

	    return $this->render('update', [
		    'supplierModel' => $supplierModel,
		    'productDataProvider' => $productDataProvider,
		    'productModels' => $products
	    ]);
    }

    /**
     * Deletes an existing Supplier model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Supplier model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Supplier the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Supplier::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
