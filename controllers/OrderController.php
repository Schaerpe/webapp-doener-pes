<?php

namespace app\controllers;

use app\models\CollectiveOrder;
use Yii;
use app\models\Order;
use app\models\search\OrderSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
	        'access' => [
	            'class' => AccessControl::className(),
		        'rules' => [
		        	[
				        'allow' => true,
				        'actions' => ['index', 'create', 'view', 'update'],
				        'roles' => ['regular'],
			        ],
			        [
			        	'allow' => true,
				        'actions' => ['delete'],
				        'roles' => ['admin'],
			        ],
		        ],
	        ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if (count(CollectiveOrder::find()->deadlineReached(false)->all()) < 1) {
        	Yii::$app->session->setFlash('info', 'Es gibt momentan keine Sammelbestellung. Bitte erstell doch eine :-)');
        	return $this->redirect(\yii\helpers\Url::to(['site/index']));
        }

        $collectiveOrders = CollectiveOrder::find()->deadlineReached(false)->all();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
	        'collectiveOrders' => $collectiveOrders,
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->user->identity->id === $model->user_id || Yii::$app->user->can('admin')) {
	        if ($model->load(Yii::$app->request->post()) && $model->save()) {
		        return $this->redirect(['view', 'id' => $model->id]);
	        }
        } else {
        	$this->refresh();
        	Yii::$app->session->setFlash('warning', 'You are not allowed to edit this order.');
        }


        return $this->render('update', [
            'model' => $model,
        ]);
    }

	/**
	 * Deletes an existing Order model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 * @return mixed
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 * @throws \yii\web\NotFoundHttpException if the model cannot be found
	 */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
