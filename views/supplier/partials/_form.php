<?php

use app\models\Ingredient;
use unclead\multipleinput\MultipleInputColumn;
use unclead\multipleinput\TabularInput;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $supplierModel app\models\Supplier */
/* @var $productModels \app\models\Product[] */
/* @var $productDataProvider \yii\data\ActiveDataProvider|null */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="supplier-form">

    <?php $form = ActiveForm::begin(); ?>
	<?php if (!is_null($productModels)) echo $form->errorSummary($productModels) ?>
	<?= $form->errorSummary($supplierModel) ?>
	<div class="form-group">
		<div class="row">
			<div class="col-sm-4">
				<?= $form->field($supplierModel, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Bsp. Alla Turca']) ?>
				<?= $form->field($supplierModel, 'address')->textInput(['maxlength' => true, 'placeholder' => 'Bsp. Hauptstrasse 12, 5034 Suhr']) ?>
			</div>
			<div class="col-sm-4">
				<?= $form->field($supplierModel, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Bsp. doener@gmail.com']) ?>
				<?= $form->field($supplierModel, 'phone')->textInput(['maxlength' => true, 'placeholder' => 'Bsp. +41887776655']) ?>
			</div>
		</div>
	</div>

	<div class="form-group">
		<h2>Produkte</h2>
		<?php if ($productDataProvider !== null) : ?>
			<?= GridView::widget([
				'dataProvider' => $productDataProvider,
				'columns' => [
					[
						'attribute' => 'name',
					],
					[
						'attribute' => 'base_price',
						'format' => 'currency',
					],
					[
						'class' => ActionColumn::className(),
						'controller' => 'product',
						'template' => '{update} {delete}',
					]
				],
			]) ?>
		<?php endif; ?>
		<?= TabularInput::widget([
			'models' => $productModels,
			'columns' => [
				[
					'name' => 'name',
					'title' => 'Produktname',
					'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
				],
				[
					'name' => 'base_price',
					'title' => 'Basispreis',
					'type' => MultipleInputColumn::TYPE_TEXT_INPUT,
				],
			],
		]) ?>
	</div>

	<div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
