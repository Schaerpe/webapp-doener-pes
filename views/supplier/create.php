<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $supplierModel app\models\Supplier */
/* @var $productModels app\models\Product[] */

$this->title = 'Create Supplier';
$this->params['breadcrumbs'][] = ['label' => 'Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="supplier-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('partials/_form', [
        'supplierModel' => $supplierModel,
	    'productModels' => $productModels,
	    'productDataProvider' => null,
    ]) ?>

</div>
