<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SupplierSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Suppliers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supplier-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Supplier', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            'email:email',
            'address',
            'phone',
            [
	            'format' => 'html',
            	'label' => 'Products',
	            'value' => function ($model) {
    	            $html = '';
					/** @var \app\models\Supplier $model */
		            /** @var \app\models\Product $product */
		            foreach ($model->products as $product) {
		            	$html = $html .
			            Html::beginTag('p') .
			            Html::a($product->name, ['product/view', 'id' => $product->id]) .
			            Html::endTag('p');
		            }
		            return $html;
	            },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
