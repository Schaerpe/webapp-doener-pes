<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $supplierModel app\models\Supplier */
/* @var $productDataProvider \yii\data\ActiveDataProvider */
/* @var $productModels \app\models\Product[] */

$this->title = 'Update Supplier: ' . $supplierModel->name;
$this->params['breadcrumbs'][] = ['label' => 'Suppliers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $supplierModel->name, 'url' => ['view', 'id' => $supplierModel->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="supplier-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('partials/_form', [
        'supplierModel' => $supplierModel,
	    'productDataProvider' => $productDataProvider,
	    'productModels' => $productModels,
    ]) ?>

</div>
