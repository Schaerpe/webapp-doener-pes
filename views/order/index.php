<?php

use app\models\Product;
use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success']) ?>
	</p>


	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			'id',
			[
				'attribute' => 'collectiveorder_id',
				'label' => 'Collective order',
				'format' => 'html',
				'value' => function($model) {
					return Html::a($model->collectiveorder_id, Url::to(['collective-order/view', 'id'=>$model->collectiveorder_id]));
				},
			],
			[
				'attribute' => 'user_id',
				'label' => 'User',
				'value' => function($model) {
					return User::findOne(['id' => $model->user_id])->short;
				},
			],
			[
				'attribute' => 'product_id',
				'label' => 'Product',
				'value' => function($model) {
					return Product::findOne(['id' => $model])->name;
				},
			],
			'total_price:currency',
			'notes',
			[
				'class' => 'yii\grid\ActionColumn'
			],
		],
	]); ?>


</div>
