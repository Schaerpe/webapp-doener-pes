<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $collectiveOrders \app\models\CollectiveOrder[] */

$collectiveOrderItems = [];
foreach ($collectiveOrders as $collectiveOrder) {
	array_push($collectiveOrderItems, [$collectiveOrder->id => $collectiveOrder->supplier->name]);
}
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'product_id')->textInput() ?>

    <?= $form->field($model, 'collectiveorder_id')->dropDownList($collectiveOrderItems) ?>

    <?= $form->field($model, 'total_price')->textInput() ?>

    <?= $form->field($model, 'notes')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
