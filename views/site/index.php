<?php

/* @var $this yii\web\View */

$this->title = 'Dönerapp';
use yii\helpers\Html;
use yii\helpers\Url; ?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Willkommen zum Dönerapp!</h1>
    </div>
	<div class="jumbotron">
		<p><a class="btn btn-lg btn-primary" href=<?= Url::to(['order/create'])?>>Jetzt Bestellen</a></p>
		<p>oder...</p>
		<p><a class="btn btn-outline-primary" href=<?= Url::to(['collective-order/create']) ?>>Sammelbestellung erstellen</a></p>
	</div>
	<div class="text-center">
		<?= Html::img('@web/images/doener.png') ?>
	</div>
</div>
