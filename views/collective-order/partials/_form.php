<?php

use app\models\Supplier;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CollectiveOrder */
/* @var $form yii\widgets\ActiveForm */

$supplierSelectItems = ArrayHelper::map(Supplier::find()->all(), 'id', 'name');
?>

<div class="collective-order-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->errorSummary($model) ?>

	<?= $form->field($model, 'supplier_id')->dropDownList($supplierSelectItems) ?>
    <?= $form->field($model, 'deadline')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
