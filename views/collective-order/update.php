<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CollectiveOrder */

$this->title = 'Update Collective Order: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Collective Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="collective-order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('partials/_form', [
        'model' => $model,
    ]) ?>

</div>
