<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CollectiveOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Collective Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collective-order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Collective Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'supplier_id',
            'responsible_user_id',
            'created_at',
            'deadline',
            //'paid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
