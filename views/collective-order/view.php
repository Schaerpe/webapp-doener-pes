<?php

use app\models\Supplier;
use app\models\User;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CollectiveOrder */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Collective Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="collective-order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
            	'label' => 'Supplier',
	            'value' => Supplier::find()->andWhere(['id'=>$model->supplier_id])->one()->name,
            ],
            [
            	'label' => 'Responsible user',
	            'value' => User::find()->andWhere(['id'=>$model->responsible_user_id])->one()->short,
            ],
            [
            	'label' => 'Created at',
	            'value' => Yii::$app->formatter->asDate($model->created_at, 'short'),
            ],
            [
            	'label' => 'Deadline',
	            'value' => Yii::$app->formatter->asTime($model->deadline, 'short'),
            ],
            [
            	'label' => 'Paid',
	            'format' => 'html',
	            'value' => $model->paid ? FA::icon('check', ['class'=>'text-success']) : FA::icon('times', ['class'=>'text-danger']),
            ],
        ],

    ]) ?>

</div>
