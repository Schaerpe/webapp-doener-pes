<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CollectiveOrder */

$this->title = 'Create Collective Order';
$this->params['breadcrumbs'][] = ['label' => 'Collective Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="collective-order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('partials/_form', [
        'model' => $model,
    ]) ?>

</div>
